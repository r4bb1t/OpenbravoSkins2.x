# Openbravo 2.x skins

Different colors for the openbravo 2.x ERP to switch from the standard green. 
Currently with two variations:

* NekoMikoRedmu

	![alt text](https://gitlab.com/r4bb1t/OpenbravoSkins2.x/raw/master/NekoMikoRedmu/Login/initialOpenbravoLogo.png "Neko Miko Redmu Logo")

* YuyukoBlue

	![alt text](https://gitlab.com/r4bb1t/OpenbravoSkins2.x/raw/master/YuyukoBlue/Login/initialOpenbravoLogo.png "Yuyuko Blue Logo")

* YellowMarisa (coming up)


#### **For a guide on how to add/replace/create a skin please check the [wiki](https://gitlab.com/r4bb1t/OpenbravoSkins2.x/wikis/home) of this project**
